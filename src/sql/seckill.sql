--秒杀执行存储过程
DELIMITER $$ -- 换行符由 ;变成$$
-- 存储过程定义
-- in 输入参数 out; 输出参数
-- row_count() 返回上一条修改类型sql(insert, update, delete)影响的行数
-- row_count() 0：表示未修改; >0：表示修改的行数; <0：表示sql错误或未执行修改sql
CREATE PROCEDURE seckill.execute_seckill
(in v_seckill_id bigint, in v_phone bigint, in v_kill_time timestamp, out r_result int)
BEGIN
	DECLARE insert_count int DEFAULT 0;
	START TRANSACTION;
	INSERT ignore INTO success_killed(seckill_id,user_phone,create_time)
		VALUES(v_seckill_id, v_phone, v_kill_time);
	SELECT row_count() INTO insert_count;
	IF(insert_count = 0) THEN
		ROLLBACK;
		SET r_result = -1;
	ELSEIF(insert_count < 0) THEN
		ROLLBACK;
		SET r_result = -2;
	ELSE
		UPDATE seckill SET number = number-1 
			WHERE seckill_id=v_seckill_id
			AND start_time<v_kill_time
			AND end_time>v_kill_time
			AND number>0;
		SELECT row_count() INTO insert_count;
		IF(insert_count = 0) THEN
			ROLLBACK;
			SET r_result = -1;
		ELSEIF(insert_count < 0) THEN
			ROLLBACK;
			SET r_result = -2;
		ELSE
			COMMIT;
			SET r_result = 1;
		END IF;
	END IF;
END;
$$
-- 存储过程定义结束

-- 下面sql在mysql中执行
DELIMITER ;
set @r_result=-3;
--执行存储过程
call execute_seckill(1003, 13912345678, now(), @r_result);
-- 获取结果
select @r_result;



		
		