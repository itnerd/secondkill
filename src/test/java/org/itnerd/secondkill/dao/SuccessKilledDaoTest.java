package org.itnerd.secondkill.dao;

import static org.junit.Assert.fail;

import javax.annotation.Resource;

import org.itnerd.secondkill.entity.SuccessKilled;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-dao.xml")
public class SuccessKilledDaoTest {
	
	@Resource
	private SuccessKilledDao sk;

	@Test
	public void insertSuccessKilled() {
		long seckillId = 1000L;
		long userPhone = 13012345678L;
		int insertCount = sk.insertSuccessKilled(seckillId, userPhone);
		System.out.println("insertCount="+insertCount);
	}
	
	@Test
	public void queryByIdWithSeckill() {
		long seckillId = 1000L;
		long userPhone = 13012345678L;
		SuccessKilled seckill = sk.queryByIdWithSeckill(seckillId, userPhone);
		System.out.println(seckill);
	}
}
