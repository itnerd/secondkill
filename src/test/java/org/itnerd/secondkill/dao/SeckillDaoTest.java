package org.itnerd.secondkill.dao;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.itnerd.secondkill.entity.Seckill;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-dao.xml")
public class SeckillDaoTest {
	
	@Resource
	private SeckillDao sd;
	
	@Test
	public void reduceNumber() {
		long seckillId=1000;
		Date killTime = new Date();
		int updateCount = sd.reduceNumber(seckillId, killTime);
		System.out.println("秒杀商品数量："+ updateCount);
	}
	
	@Test
	public void queryById() {
		long seckillId = 1000L;
		Seckill seckill = sd.queryById(seckillId);
		System.out.println(seckill);
	}
	
	@Test
	public void queryAll() {
		int offset = 0;
		int limit = 100;
		List<Seckill> list = sd.queryAll(offset, limit);
		for(Seckill s : list) {
			System.out.println(s);
		}
	}
	
}
