package org.itnerd.secondkill.service;

import java.util.List;

import org.itnerd.secondkill.dto.Exposer;
import org.itnerd.secondkill.dto.SeckillExecution;
import org.itnerd.secondkill.entity.Seckill;
import org.itnerd.secondkill.exception.RepeatKillException;
import org.itnerd.secondkill.exception.SeckillCloseException;
import org.itnerd.secondkill.exception.SeckillException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/spring-dao.xml","classpath:spring/spring-service.xml"})
public class SeckillServiceTest {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SeckillService seckillService;
	
	@Test
	public void getSeckillList() {
		List<Seckill> seckillList = seckillService.getSeckillList();
		logger.info("seckillList={}", seckillList);
	}

	@Test
	public void getById() {
		long seckillId = 1000L;
		Seckill seckill = seckillService.getById(seckillId);
		logger.info("seckill={}", seckill);
	}
	
	@Test
	public void testSeckillLogic() {
		long seckillId = 1001;
		Exposer exposer = seckillService.exportSeckillUrl(seckillId);
		if(exposer.isExposed()) {
			logger.info("exposer: {}", exposer);
			long userPhone = 13012345678L;
			String md5 = exposer.getMd5();
			
			try {
				SeckillExecution executeSeckill = seckillService.executeSeckill(seckillId, userPhone, md5);
				logger.info("秒杀结果：{}", executeSeckill);
			} catch (RepeatKillException e) {
				e.printStackTrace();
			} catch (SeckillException e) {
				e.printStackTrace();
			} catch (SeckillCloseException e) {
				e.printStackTrace();
			}
		} else {
			//秒杀未开启
			logger.info("exposer: {}", exposer);
		}
	}
}
