package org.itnerd.secondkill.dto;

/** 
 * @ClassName: SeckillResult 
 * @Description: 将所有的ajax的对象封装成json
 * @author zwh
 * @version V1.0 
 */
public class SeckillResult<T> {
	
	private boolean success;	//访问是否成功
	private T data;
	private String error;
	
	public SeckillResult(boolean success, T data) {
		super();
		this.success = success;
		this.data = data;
	}

	public SeckillResult(boolean success, String error) {
		super();
		this.success = success;
		this.error = error;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
}
