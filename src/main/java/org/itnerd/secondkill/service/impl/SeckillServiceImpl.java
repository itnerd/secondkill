package org.itnerd.secondkill.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.itnerd.secondkill.dao.SeckillDao;
import org.itnerd.secondkill.dao.SuccessKilledDao;
import org.itnerd.secondkill.dto.Exposer;
import org.itnerd.secondkill.dto.SeckillExecution;
import org.itnerd.secondkill.entity.Seckill;
import org.itnerd.secondkill.entity.SuccessKilled;
import org.itnerd.secondkill.enums.SeckillStatEnum;
import org.itnerd.secondkill.exception.RepeatKillException;
import org.itnerd.secondkill.exception.SeckillCloseException;
import org.itnerd.secondkill.exception.SeckillException;
import org.itnerd.secondkill.service.SeckillService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

/** 
 * @ClassName: SeckillServiceImpl 
 * @Description: 秒杀业务接口实现类
 * @author zwh
 * @version V1.0 
 */
@Service
public class SeckillServiceImpl implements SeckillService {
	//日志对象
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	//加入一个混淆字符串(秒杀接口)的salt
	private final String salt = "fwiqnvcxoi328@364*^))23-=+nv3@438!";
	
	@Autowired
	private SeckillDao sd;
	
	@Autowired
	private SuccessKilledDao skd;

	@Override
	public List<Seckill> getSeckillList() {
		return sd.queryAll(0, 4);
	}

	@Override
	public Seckill getById(long seckillId) {
		return sd.queryById(seckillId);
	}

	@Override
	public Exposer exportSeckillUrl(long seckillId) {
		Seckill seckill = getById(seckillId);
		if(seckill == null) {
			new Exposer(false, seckillId);
		}
		Date nowTime = new Date();	//系统当前时间
		Date startTime = seckill.getStartTime();
		Date endTime = seckill.getEndTime();
		
		if(startTime.getTime() > nowTime.getTime() || endTime.getTime() < nowTime.getTime()) {
			new Exposer(false, seckillId, nowTime.getTime(), startTime.getTime(), endTime.getTime());
		}
		
		String md5 = getMD5(seckillId);
		return new Exposer(true, md5, seckillId);
	}

	/** 
	 * Title: getMD5<br>
	 * Description: 根据秒杀商品id生成md5值
	 * @param seckillId
	 * @return String    返回秒杀商品id的md5值 
	 */
	public String getMD5(long seckillId) {
		String base = seckillId + "/" + salt;
		String md5 = DigestUtils.md5DigestAsHex(base.getBytes());
		return md5;
	}

	@Override
	@Transactional
	public SeckillExecution executeSeckill(long seckillId, long userPhone, String md5)
			throws SeckillException, RepeatKillException, SeckillCloseException {
		if (md5 == null || !md5.equals(getMD5(seckillId))) {
            //秒杀数据被重写了
            throw new SeckillException("seckill data rewrite");
        }
        //执行秒杀逻辑:减库存+增加购买明细
        Date nowTime = new Date();

        try {
            //否则更新了库存，秒杀成功,增加明细
            int insertCount = skd.insertSuccessKilled(seckillId, userPhone);
            //看是否该明细被重复插入，即用户是否重复秒杀
            if (insertCount <= 0) {
                throw new RepeatKillException("seckill repeated");
            } else {
                //减库存,热点商品竞争
                int updateCount = sd.reduceNumber(seckillId, nowTime);
                if (updateCount <= 0) {
                    //没有更新库存记录，说明秒杀结束 rollback
                    throw new SeckillCloseException("seckill is closed");
                } else {
                    //秒杀成功,得到成功插入的明细记录,并返回成功秒杀的信息 commit
                    SuccessKilled successKilled = skd.queryByIdWithSeckill(seckillId, userPhone);
                    return new SeckillExecution(seckillId, SeckillStatEnum.SUCCESS, successKilled);
                }
            }
        } catch (SeckillCloseException e1) {
            throw e1;
        } catch (RepeatKillException e2) {
            throw e2;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            //所以编译期异常转化为运行期异常
            throw new SeckillException("seckill inner error :" + e.getMessage());
        }
	}

	public SeckillExecution executeSeckillByProcedure(long seckillId, long userPhone, String md5) {
		if (md5 == null || !md5.equals(getMD5(seckillId))) {
            //秒杀数据被重写了
            return new SeckillExecution(seckillId, SeckillStatEnum.DATE_REWRITE);
        }
		Date killTime = new Date();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("seckillId", seckillId);
		map.put("phone", userPhone);
		map.put("killTime", killTime);
		map.put("result", null);
		
		try {
			sd.killByProcedure(map);
			Integer result = MapUtils.getInteger(map, "result", -2);
			if(result == 1) {
				SuccessKilled sk = skd.queryByIdWithSeckill(seckillId, userPhone);
				return new SeckillExecution(seckillId, SeckillStatEnum.SUCCESS, sk);
			} else {
				return new SeckillExecution(seckillId, SeckillStatEnum.stateOf(result));
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return new SeckillExecution(seckillId, SeckillStatEnum.INNER_ERROR);
		}
		
	}
	
}
