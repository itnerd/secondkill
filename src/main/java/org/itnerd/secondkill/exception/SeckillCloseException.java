package org.itnerd.secondkill.exception;

/** 
 * @ClassName: SeckillCloseException 
 * @Description: 秒杀关闭异常
 * @author zwh
 * @version V1.0 
 */
public class SeckillCloseException extends Exception {

	public SeckillCloseException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SeckillCloseException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
