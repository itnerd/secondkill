package org.itnerd.secondkill.exception;

/** 
 * @ClassName: SeckillException 
 * @Description: 秒杀异常
 * @author zwh
 * @version V1.0 
 */
public class SeckillException extends Exception {

	public SeckillException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SeckillException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
