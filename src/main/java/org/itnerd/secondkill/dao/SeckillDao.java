package org.itnerd.secondkill.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.itnerd.secondkill.entity.Seckill;

/** 
 * @ClassName: SeckillDao 
 * @Description: 秒杀商品接口
 * @author zwh
 * @version V1.0 
 */
public interface SeckillDao {
	/**
     * 减库存
     * @param seckillId
     * @param killTime
     * @return 如果影响行数>1，表示更新库存的记录行数
     */
    int reduceNumber(@Param("seckillId")long seckillId, @Param("killTime") Date killTime);

    /**
     * 根据id查询秒杀的商品信息
     * @param seckillId
     * @return
     */
    Seckill queryById(long seckillId);

    /**
     * 分页查询秒杀商品列表
     * @param offset
     * @param limit
     * @return
     */
    List<Seckill> queryAll(@Param("offset") int offset, @Param("limit")int limit);
    
    /**
     * Title: killByProcedure<br>
     * Description: 在存储过程中执行秒杀
     * @param paramMap
     * @return void    返回类型
     */
    void killByProcedure(Map<String, Object> paramMap);
}
